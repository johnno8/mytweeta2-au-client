/**
 * Created by John on 04/01/2017.
 */

import {inject} from 'aurelia-framework';
import Fixtures from './fixtures';
import {EventAggregator} from 'aurelia-event-aggregator';
import {LoginStatus} from './messages';

@inject(Fixtures, EventAggregator)
export default class MytweetService {

  tweets = [];
  users = [];
  loggedInUser = {};

  constructor(data, ea) {
    this.users = data.users;
    this.tweets = data.tweets;
    this.ea = ea;
  }

  posttweet(content) {
    let d = new Date();
    let datestring = ('0' + d.getDate()).slice(-2) + '-' + ('0' + (d.getMonth() + 1)).slice(-2) +
      '-' + d.getFullYear() + ' ' + ('0' + d.getHours()).slice(-2) + ':' +
      ('0' + d.getMinutes()).slice(-2);
    let tweet = {
      content: content,
      tweetor: this.loggedInUser.email,
      date: datestring
    };
    this.tweets.push(tweet);
    console.log('Tweet with content: \'' + content + '\' sent at: ' + datestring);
  }

  login(email, password) {
    const status = {
      success: false,
      message: ''
    };

    if (this.users[email]) {
      if (this.users[email].password === password) {
        status.success = true;
        status.message = 'logged in';
        this.loggedInUser = this.users[email];
      } else {
        status.message = 'Incorrect password';
      }
    } else {
      status.message = 'Unknown user';
    }

    this.ea.publish(new LoginStatus(status));
  }

  register(firstName, lastName, email, password) {
    const newUser = {
      firstName: firstName,
      lastName: lastName,
      email: email,
      password: password
    };
    this.users[email] = newUser;
  }

  logout() {
    const status = {
      success: false,
      message: ''
    };
    this.ea.publish(new LoginStatus(status));
  }
}
