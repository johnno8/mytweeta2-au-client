/**
 * Created by John on 04/01/2017.
 */

export default class Fixtures {
  tweets = [
    {
      content: 'Tweet1',
      tweetor: 'homer@simpson.com',
      date: '03-01-2017 15:33'
    },
    {
      content: 'Tweet2',
      tweetor: 'marge@simpson.com',
      date: '04-01-2017 15:34'
    },
    {
      content: 'Tweet3',
      tweetor: 'bart@simpson.com',
      date: '04-01-2017 16:44'
    },
    {
      content: 'Tweet4',
      tweetor: 'barney@gumble.com',
      date: '04-01-2017 16:51'
    },
    {
      content: 'Tweet5',
      tweetor: 'lisa@simpson.com',
      date: '04-01-2017 17:02'
    }
  ];

  users = {
    'homer@simpson.com': {
      firstName: 'Homer',
      lastName: 'Simpson',
      email: 'homer@simpson.com',
      password: 'secret'
    },
    'marge@simpson.com': {
      firstName: 'Marge',
      lastName: 'Simpson',
      email: 'marge@simpson.com',
      password: 'secret1'
    },
    'bart@simpson.com': {
      firstName: 'Bart',
      lastName: 'Simpson',
      email: 'bart@simpson.com',
      password: 'secret2'
    },
    'lisa@simpson.com': {
      firstName: 'Lisa',
      lastName: 'Simpson',
      email: 'lisa@simpson.com',
      password: 'secret3'
    },
    'barney@gumble.com': {
      firstName: 'Barney',
      lastName: 'gumble',
      email: 'barney@gumble.com',
      password: 'secret4'
    }
  }
}
