/**
 * Created by John on 06/01/2017.
 */

import {inject} from 'aurelia-framework';
import MytweetService from '../../services/mytweet-service';

@inject(MytweetService)
export class Signup {

  firstName = '';
  lastName = '';
  email = '';
  password = '';

  constructor(ms) {
    this.mytweetService = ms;
  }

  register(e) {
    this.showSignup = false;
    this.mytweetService.register(this.firstName, this.lastName, this.email, this.password);
    this.mytweetService.login(this.email, this.password);
  }
}
