/**
 * Created by John on 04/01/2017.
 */

import {inject} from 'aurelia-framework';
import MytweetService from '../../services/mytweet-service';

@inject(MytweetService)
export class Tweet {

  content = '';

  constructor(ms) {
    this.mytweetService = ms;
    this.content = ms.content;
  }

  sendTweet() {
    this.mytweetService.posttweet(this.content);
    this.content = '';
  }
}
