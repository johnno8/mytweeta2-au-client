/**
 * Created by John on 04/01/2017.
 */

import {inject} from 'aurelia-framework';
import MytweetService from '../../services/mytweet-service';

@inject(MytweetService)
export class Timeline {

  tweets = [];

  constructor(ms) {
    this.mytweetService = ms;
    this.tweets = this.mytweetService.tweets;
  }
}
