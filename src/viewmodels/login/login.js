/**
 * Created by John on 06/01/2017.
 */

import {inject} from 'aurelia-framework';
import MytweetService from '../../services/mytweet-service';

@inject(MytweetService)
export class Login {

  email = '';
  password = '';

  constructor(ms) {
    this.mytweetService = ms;
    this.prompt = '';
  }

  login(e) {
    console.log(`Trying to log in ${this.email}`);
    this.mytweetService.login(this.email, this.password);
  }
}
