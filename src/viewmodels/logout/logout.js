/**
 * Created by John on 07/01/2017.
 */
import MytweetService from '../../services/mytweet-service';
import {inject} from 'aurelia-framework';

@inject(MytweetService)
export class Logout {

  constructor(mytweetService) {
    this.mytweetService = mytweetService;
  }

  logout() {
    console.log('logging out');
    this.mytweetService.logout();
  }
}
